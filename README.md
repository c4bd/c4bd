# Computing for Big Data (c4bd)

## A health data science perspective

http://c4bd.gitlab.io/c4bd

Big data is everywhere, from omics and health policy to environmental health. Every single aspect of the health sciences is being transformed. It is hard to navigate and critically assess tools and techniques in such a fast-moving big data panorama. This course gives a critical presentation of theoretical approaches and software implementations of tools to collect, store, and process data at scale. The goal is not just to learn recipes to manipulate big data, but to learn how to reason in terms of big data, from software design and tool selection to implementation, optimization, and maintenance.

We will see how big data changes several aspects of data science (for instance, data management, software development and visualization) and how we can leverage dedicated tools to work with big data efficiently.

**Part I: Computing**

Module 1: Introduction

https://renkulab.io/projects/c4bd/module1/

Module 2: Software engineering

https://renkulab.io/projects/c4bd/module2/

Module 3: Improving performance

https://renkulab.io/projects/c4bd/module3/

**Part II: Big Data**

Module 4: Distributed computing

https://renkulab.io/projects/c4bd/module4/

Module 5: Databases

https://renkulab.io/projects/c4bd/module5/

**Part III: Computing for Big Data**

Module 6: Big data stacks

https://renkulab.io/projects/c4bd/module6/

<div>Icons made by <a href="https://creativemarket.com/eucalyp" title="Eucalyp">Eucalyp</a> from <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a></div>
